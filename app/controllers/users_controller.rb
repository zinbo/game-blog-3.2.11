class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(params[:user])
    if @user.email == "shane@zinbo.co.uk"
      if @user.save
        sign_in @user
        flash[:success] = "Welcome to the Sample App!"
        redirect_to root_url
      else
        render 'new'
      end  
      else
        flash.now[:error] = 'Registrations are not taking place right now.'
        render 'new'
    end
    
  end
end
