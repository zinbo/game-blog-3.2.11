# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{hublot}
  s.version = "0.0.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Brett Shollenberger"]
  s.date = %q{2013-07-19}
  s.description = %q{Humanizes datetime for Active Record objects.}
  s.email = ["brett.shollenberger@gmail.com"]
  s.files = ["test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/models/comment.rb", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/secret_token.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/config.ru", "test/dummy/db/development.sqlite3", "test/dummy/db/migrate/20130715211742_create_comments.rb", "test/dummy/db/schema.rb", "test/dummy/db/test.sqlite3", "test/dummy/Guardfile", "test/dummy/log/development.log", "test/dummy/log/test.log", "test/dummy/Rakefile", "test/dummy/script/rails", "test/dummy/spec/factories.rb", "test/dummy/spec/features/hublot_prettifies_time_with_zone_spec.rb", "test/dummy/spec/models/comment_spec.rb", "test/dummy/spec/spec_helper.rb", "test/dummy/tmp/rspec_guard_result"]
  s.homepage = %q{http://github.com/brettshollenberger/hublot}
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.6.2}
  s.summary = %q{Adds datetime humanization to Active Record objects.}
  s.test_files = ["test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/models/comment.rb", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/secret_token.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/config.ru", "test/dummy/db/development.sqlite3", "test/dummy/db/migrate/20130715211742_create_comments.rb", "test/dummy/db/schema.rb", "test/dummy/db/test.sqlite3", "test/dummy/Guardfile", "test/dummy/log/development.log", "test/dummy/log/test.log", "test/dummy/Rakefile", "test/dummy/script/rails", "test/dummy/spec/factories.rb", "test/dummy/spec/features/hublot_prettifies_time_with_zone_spec.rb", "test/dummy/spec/models/comment_spec.rb", "test/dummy/spec/spec_helper.rb", "test/dummy/tmp/rspec_guard_result"]

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, ["> 3.0.0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<activesupport>, [">= 0"])
      s.add_development_dependency(%q<i18n>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<rails>, ["> 3.0.0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<activesupport>, [">= 0"])
      s.add_dependency(%q<i18n>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, ["> 3.0.0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<activesupport>, [">= 0"])
    s.add_dependency(%q<i18n>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
