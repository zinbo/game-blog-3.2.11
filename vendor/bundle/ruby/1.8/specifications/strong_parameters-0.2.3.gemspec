# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{strong_parameters}
  s.version = "0.2.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["David Heinemeier Hansson"]
  s.date = %q{2014-01-20}
  s.email = ["david@heinemeierhansson.com"]
  s.files = ["test/action_controller_required_params_test.rb", "test/action_controller_tainted_params_test.rb", "test/active_model_mass_assignment_taint_protection_test.rb", "test/controller_generator_test.rb", "test/gemfiles/Gemfile.rails-3.0.x", "test/gemfiles/Gemfile.rails-3.0.x.lock", "test/gemfiles/Gemfile.rails-3.1.x", "test/gemfiles/Gemfile.rails-3.2.x", "test/log_on_unpermitted_params_test.rb", "test/multi_parameter_attributes_test.rb", "test/parameters_permit_test.rb", "test/parameters_require_test.rb", "test/parameters_taint_test.rb", "test/raise_on_unpermitted_params_test.rb", "test/test_helper.rb"]
  s.homepage = %q{https://github.com/rails/strong_parameters}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.6.2}
  s.summary = %q{Permitted and required parameters for Action Pack}
  s.test_files = ["test/action_controller_required_params_test.rb", "test/action_controller_tainted_params_test.rb", "test/active_model_mass_assignment_taint_protection_test.rb", "test/controller_generator_test.rb", "test/gemfiles/Gemfile.rails-3.0.x", "test/gemfiles/Gemfile.rails-3.0.x.lock", "test/gemfiles/Gemfile.rails-3.1.x", "test/gemfiles/Gemfile.rails-3.2.x", "test/log_on_unpermitted_params_test.rb", "test/multi_parameter_attributes_test.rb", "test/parameters_permit_test.rb", "test/parameters_require_test.rb", "test/parameters_taint_test.rb", "test/raise_on_unpermitted_params_test.rb", "test/test_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>, ["~> 3.0"])
      s.add_runtime_dependency(%q<actionpack>, ["~> 3.0"])
      s.add_runtime_dependency(%q<activemodel>, ["~> 3.0"])
      s.add_runtime_dependency(%q<railties>, ["~> 3.0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
    else
      s.add_dependency(%q<activesupport>, ["~> 3.0"])
      s.add_dependency(%q<actionpack>, ["~> 3.0"])
      s.add_dependency(%q<activemodel>, ["~> 3.0"])
      s.add_dependency(%q<railties>, ["~> 3.0"])
      s.add_dependency(%q<rake>, [">= 0"])
    end
  else
    s.add_dependency(%q<activesupport>, ["~> 3.0"])
    s.add_dependency(%q<actionpack>, ["~> 3.0"])
    s.add_dependency(%q<activemodel>, ["~> 3.0"])
    s.add_dependency(%q<railties>, ["~> 3.0"])
    s.add_dependency(%q<rake>, [">= 0"])
  end
end
