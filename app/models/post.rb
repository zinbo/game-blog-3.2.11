class Post < ActiveRecord::Base
  include ActiveModel::ForbiddenAttributesProtection
  validates_presence_of :body, :title
  has_many :comments
end
