class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create!(user_params)
    respond_to do |format|
        format.html { redirect_to post_path(@post) }
        format.js 
    end
  end
  
  private
  def user_params
      params.require(:comment).permit(:user, :post, :body)
  end
end
